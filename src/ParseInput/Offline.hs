module ParseInput.Offline where

import ParseInput.Common

import Text.Megaparsec hiding (many, some)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer qualified as L
import Std

offlineInput :: Parse (Maybe Input)
offlineInput =
  -- just ignore the compose stuff
  getCompose
    (
      (
        Compose $
        fmap Just $
        Input
          <$> globalParameter
          <*> globalParameter
          <*> globalParameter
          <* globalParameter
      )
          <*> Compose (nonEmpty <$> many patient)
    ) <*
  eof

parseOfflineInput :: FilePath -> Text -> IO (Maybe Input)
parseOfflineInput =
  either (throwString . errorBundlePretty) pure .: runParser offlineInput
