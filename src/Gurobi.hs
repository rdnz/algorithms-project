module Gurobi where

import System.Process.Typed (proc, runProcess_, readProcessStdout_, setWorkingDir)
import Path.IO (withTempDir, createTempDir, getCurrentDir)
import Path (parseRelFile, fromAbsDir, fromAbsFile, (</>))
import System.Exit (ExitCode (ExitSuccess, ExitFailure))
import Text.Megaparsec hiding (many, some)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer qualified as L
import Data.Char (isSpace)
import Std hiding (try)

type Parse = Parsec Void Text

solve :: Bool -> Text -> IO (Integer, HashMap Text Integer, Double)
solve verbose model =
  do
    currentDir <- getCurrentDir
    -- createTempDir currentDir "gurobi" >>= \tempDir ->
    withTempDir currentDir "gurobi" $ \tempDir ->
      do
        modelFile <- parseRelFile "model.lp"
        when verbose $ putTextLn "writing model"
        writeFileUtf8 (fromAbsFile $ tempDir </> modelFile) model
        (
          (if verbose then runProcess_ else void . readProcessStdout_) $
          setWorkingDir (fromAbsDir tempDir) $
          proc "gurobi_cl" ["ResultFile=solution.sol", "model.lp"]
          )
        solutionFile <- parseRelFile "solution.sol"
        logFile <- parseRelFile "gurobi.log"
        (objective, assignment) <-
          parseSolution "solution.sol" =<<
            readFileUtf8 (fromAbsFile $ tempDir </> solutionFile)
        runTimeResult <-
          parseRunTime "gurobi.log" =<<
            readFileUtf8 (fromAbsFile $ tempDir </> logFile)
        pure (objective, assignment, runTimeResult)

integer :: Parse Integer
integer = L.signed (L.space hspace1 empty empty) L.decimal

solution :: Parse (Integer, HashMap Text Integer)
solution =
  (,)
    <$>
      (
        string "# Objective value = " *>
        integer
      )
    <* eol
    <*>
      (fromList <$>
        many
          ((,)
            <$> takeWhile1P (Just "alpha num character") (not . isSpace)
            <* char ' '
            <*> integer
            <* eol
          )
      )
  <*
  eof

parseSolution ::
  FilePath -> Text -> IO (Integer, HashMap Text Integer)
parseSolution =
  either (throwString . errorBundlePretty) pure .: runParser solution

parseRunTime ::
  FilePath -> Text -> IO Double
parseRunTime =
  either (throwString . errorBundlePretty) pure .: runParser runTime

runTime :: Parse Double
runTime =
  (+)
    <$>
      skipManyTill
        line
        (string "Root relaxation: " *> skipManyTill anySingle time <* eol)
    <*>
      skipManyTill
        line
        (string "Explored " *> skipManyTill anySingle time <* eol)

time :: Parse Double
time = try (L.float <* string " seconds")

line :: Parse ()
line =
  takeWhileP
    (Just "non space character")
    (\c -> c != '\n' && c != '\r') *>
  eol *>
  pure ()
