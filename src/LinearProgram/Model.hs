module LinearProgram.Model
  (module LinearProgram.Model, module ParseInput.Common)
  where

import ParseInput.Common
  (
    Input (Input),
    Patient
      (Patient, interval0Start, interval0Length, delay, interval1Length),
    Time (Time),
  )
import DisplayTimetable (Machine (Machine))
import LinearProgramming

import Data.List.NonEmpty qualified as N (zip, filter)
import Data.Text.Lazy.Builder qualified as TB
import Data.Text.Lazy.Builder.Int (decimal)
import Std hiding (sum)

newtype PatientId =
  PatientId Natural
  deriving newtype (Eq, Ord, Show)

-- to-do
--   optimization for tight intervals

model :: Input -> Text
model (Input processing0 processing1 gap patients) =
  toStrict $
  TB.toLazyText $
  (
    "min\n" <>
    objective <> "\n" <>
    "such that\n" <>
    foldMap (<> "\n") activeIfUsed <>
    foldMap (<> "\n") noCollision <>
    foldMap (<> "\n") doses0 <>
    foldMap (<> "\n") doses1 <>
    foldMap (<> "\n") dosesDependencies <>
    "binary\n" <> intercalate " " binaryVariables <> "\n" <>
    "end\n"
  )
  where

  objective = sum [active m | m <- machines]

  -- variables

  active (Machine m) = " m" <> decimal m <> " "

  appointment0 (Machine m) (Time t) (PatientId p) =
    " a" <> decimal m <> "_" <> decimal t <> "_" <> decimal p <> " "

  appointment1 (Machine m) (Time t) (PatientId p) =
    " b" <> decimal m <> "_" <> decimal t <> "_" <> decimal p <> " "

  -- constraints

  activeIfUsed = do
    machine <- machines
    pure $
      "- " <> decimal (2 * length patients) <>
      active machine `plus`
      sum
        (
          [
            appointment0 machine t p
          |
            (t, p) <- timedPatientIds0
          ]
          <>
          [
            appointment1 machine t p
          |
            (t, p) <- timedPatientIds1
          ]
        )
      `lessOrEquals`
      decimal 0

  noCollision = do
    machine <- toList machines
    time <- toList times
    let
      appointments =
        [
          appointment0 machine t p
        |
          (t, p) <- toList timedPatientIds0,
          t <= time,
          time < t + processing0
        ]
        <>
        [
          appointment1 machine t p
        |
          (t, p) <- toList timedPatientIds1,
          t <= time,
          time < t + processing1
        ]
    case nonEmpty appointments of
      Just as | length as >= 2 -> pure $ sum as `lessOrEquals` decimal 1
      _ -> []

  doses0 = do
    patient <- patientIds
    pure $
      sum
        (
          fromMaybe (error $ "no feasable first appointment for patient " <> show patient) $
          nonEmpty $
          [
            appointment0 m t p
          |
            (t, p) <- toList timedPatientIds0, p == patient, m <- toList machines
          ]
        )
      `equals`
      decimal 1

  doses1 = do
    patient <- patientIds
    pure $
      sum
        (
          fromMaybe (error $ "no feasable second appointment for patient " <> show patient) $
          nonEmpty $
          [
            appointment1 m t p
          |
            (t, p) <- toList timedPatientIds1, p == patient, m <- toList machines
          ]
        )
      `equals`
      decimal 1

  dosesDependencies = do
    (t0, patient) <- toList timedPatientIds0
    let
      Patient _ _ delay interval1Length =
        fromMaybe (error "impossible because there is a patient for every id") $
        toList patients !!? fromIntegral (coerce patient :: Natural)
    case
      fmap sum $
      nonEmpty $
      [
        appointment1 m t1 p
      |
        (t1, p) <- toList timedPatientIds1,
        p == patient,
        not
          (
            t0 + processing0 + gap + delay <= t1
            &&
            t1 < t0 + processing0 + gap + delay + interval1Length - processing1 + 1
          ),
        m <- toList machines
      ]
      of
        Nothing -> []
        Just infeasableAppointments -> do
          machine <- toList machines
          pure $
            appointment0 machine t0 patient `plus`
            infeasableAppointments `lessOrEquals`
            decimal 1

  -- helpers

  timedPatientIds0 :: NonEmpty (Time, PatientId)
  timedPatientIds0 =
    fromMaybe (error "not a single feasable first appointment") $
    nonEmpty $
    [(t, pId) | t <- toList times, pId <- potentialPatientIds t]
    where
      potentialPatientIds :: Time -> [PatientId]
      potentialPatientIds time =
        fmap fst $
        N.filter
          (\(_, Patient interval0Start interval0Length _ _) ->
            interval0Start <= time
            &&
            time < interval0Start + interval0Length - processing0 + 1
          ) $
        N.zip patientIds $
        patients

  timedPatientIds1 :: NonEmpty (Time, PatientId)
  timedPatientIds1 =
    fromMaybe (error "not a single feasable first appointment") $
    nonEmpty $
    [(t, pId) | t <- toList times, pId <- potentialPatientIds t]
    where
      potentialPatientIds :: Time -> [PatientId]
      potentialPatientIds time =
        fmap fst $
        N.filter
          (\(_, Patient interval0Start interval0Length delay interval1Length) ->
            interval0Start + processing0 + gap + delay <= time
            &&
            time < interval0Start + interval0Length + gap + delay + interval1Length - processing1 + 1
          ) $
        N.zip patientIds $
        patients

  binaryVariables =
    [active m | m <- machines] <>
    [
      appointment0 m t p
    |
      m <- machines, (t, p) <- timedPatientIds0
    ] <>
    [
      appointment1 m t p
    |
      m <- machines, (t, p) <- timedPatientIds1
    ]

  patientIds =
    fmap PatientId $
    fmap fst $
    N.zip indexes $
    patients

  machines =
    fmap Machine $
    fmap fst $
    N.zip indexes $
    patients

  times =
    fromMaybe (error "processing times must be at least 1") $
    nonEmpty $
    fromTo
      0
      (
        maximum1 $
        fmap
          (\p ->
            interval0Start p + interval0Length p +
            gap + delay p + interval1Length p
          ) $
        patients
      )

indexes :: (Integral a) => NonEmpty a
indexes = 0 :| [1..]
