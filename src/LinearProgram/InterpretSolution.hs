module LinearProgram.InterpretSolution where

import ParseInput.Common (Time (Time))
import DisplayTimetable
  (Machine (Machine), ScheduledPatient (ScheduledPatient))
import LinearProgram.Model (PatientId (PatientId))

import Data.Text qualified as T
import Data.List (groupBy, lookup)
import Text.Megaparsec hiding (many, some)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer qualified as L
import Data.HashMap.Strict qualified as HM
import Std

type Parse = Parsec Void Text

schedulePatients :: HashMap Text Integer -> [ScheduledPatient]
schedulePatients values =
  zipPreciselyWith
    toScheduledPatient
    (normalizeMachineId <$> appointments0)
    (normalizeMachineId <$> appointments1)
  where
    appointments0 =
      sortOn (\(_m, _t, patient) -> patient) $
      fmap parseAppointmentDetails $
      filter (\t -> T.head t == 'a') $
      variables
    appointments1 =
      sortOn (\(_m, _t, patient) -> patient) $
      fmap parseAppointmentDetails $
      filter (\t -> T.head t == 'b') $
      variables
    normalizeMachineId (machine, t, p) =
      (
        fromMaybe (error "impossible by machines' definition") $
          lookup machine machines,
        t,
        p
      )
    machines =
      zip
        (
          hashNub $
          fmap (\(machine, _t, _p) -> machine) $
          appointments0 <> appointments1
        )
        (Machine <$> [1..])
    variables = HM.keys (HM.filter (== 1) values)

{-# NOINLINE [1] zipPreciselyWith #-}  -- See Note [Fusion for zipN/zipWithN]
zipPreciselyWith :: (Show a, Show b) => (a->b->c) -> [a]->[b]->[c]
zipPreciselyWith f = go
  where
    go [] [] = []
    go [] a = error (show a)
    go a [] = error (show a)
    go (x:xs) (y:ys) = f x y : go xs ys

toScheduledPatient ::
  (Machine, Time, PatientId) ->
  (Machine, Time, PatientId) ->
  ScheduledPatient
toScheduledPatient (machine0, time0, _) (machine1, time1, _) =
  ScheduledPatient time0 machine0 time1 machine1

appointmentDetails :: Parse (Machine, Time, PatientId)
appointmentDetails =
  (char 'a' <|> char 'b')
  *>
  ((,,)
    <$> (Machine <$> L.decimal)
    <* char '_'
    <*> L.decimal
    <* char '_'
    <*> (PatientId <$> L.decimal)
  )

parseAppointmentDetails :: Text -> (Machine, Time, PatientId)
parseAppointmentDetails =
  either (error . toText . errorBundlePretty) id
  .
  runParser appointmentDetails ""
