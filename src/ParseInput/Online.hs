module ParseInput.Online where

import ParseInput.Common

import Text.Megaparsec hiding (many, some)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer qualified as L
import Std

onlineInput :: Parse (Maybe Input)
onlineInput =
  -- just ignore the compose stuff
  getCompose
    (
      (
        Compose $
        fmap Just $
        Input
          <$> globalParameter
          <*> globalParameter
          <*> globalParameter
      )
          <*> Compose (nonEmpty <$> many patient)
    ) <*
  lexeme (char 'x') <*
  eof

parseOnlineInput :: FilePath -> Text -> IO (Maybe Input)
parseOnlineInput =
  either (throwString . errorBundlePretty) pure .: runParser onlineInput
