module ParseInput.Either where

import ParseInput.Common
import ParseInput.Offline
import ParseInput.Online

import Text.Megaparsec hiding (many, some)
import Std

parseInput :: FilePath -> Text -> IO (Maybe Input)
parseInput file text =
  either throwString pure $
  case
    (
      first errorBundlePretty $ runParser onlineInput file text
      ,
      first errorBundlePretty $ runParser offlineInput file text
    )
  of
    (Right a, _) -> Right a
    (_, Right a) -> Right a
    (Left e0, Left e1) -> Left (e0 <> "\n" <> e1)
