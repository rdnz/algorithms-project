module OnlineAlgorithm where

import ParseInput.Common (Patient (Patient), Time, Input (Input))
import DisplayTimetable
  (ScheduledPatient (ScheduledPatient), Machine (Machine))

import Data.HashSet qualified as HS
import Std hiding (sequence)

newtype Timetable = Timetable (HashSet (Time, Machine))

go :: Input -> ([ScheduledPatient], Integer)
go (Input processing0 processing1 gap patients) =
  second
    (fromIntegral . countMachines)
    (runState
      (traverse (state . processPatient) (toList patients))
      timetableEmpty
    )
  where

  processPatient :: Patient -> Timetable -> (ScheduledPatient, Timetable)
  processPatient patient timetable =
    minimum $
    sortNonEmptyOn
      (\((ScheduledPatient _t0 (Machine m0) _t1 (Machine m1)), _) ->
        m0^2 + m1^2
      ) $
    fmap (add2 timetable) $
    feasableAppointmentPairs patient
    where
      machineCount = countMachines timetable
      minimum list =
        fromMaybe
          (head list)
          (find (\(_, t) -> countMachines t == machineCount) list)

  feasableAppointmentPairs :: Patient -> NonEmpty (Time, Time)
  feasableAppointmentPairs
    (Patient interval0Start interval0Length delay interval1Length)
    =
      fromMaybe (error "at least one interval is too short") $
      nonEmpty $
      [
        (t0, t1)
      |
        t0 <- fromTo interval0Start (interval0Start + interval0Length - processing0 + 1),
        t1 <-
          fromTo
            (t0 + processing0 + gap + delay)
            (t0 + processing0 + gap + delay + interval1Length - processing1 + 1)
      ]

  add2 :: Timetable -> (Time, Time) -> (ScheduledPatient, Timetable)
  add2 timetable (time0, time1) =
    (\(m0, (m1, t)) -> (ScheduledPatient time0 m0 time1 m1, t)) $
    fmap (add time1 processing1) $
    add time0 processing0 $
    timetable

add :: Time -> Time -> Timetable -> (Machine, Timetable)
add time processing (Timetable timetable) =
  insertAndPair $
  fromMaybe (error "impossible because all machine ids are tried") $
  find (\(t, m) -> all (free m) (fromTo t (t + processing))) $
  fmap (time,) $
  fmap Machine $
  [1..]
  where
    free m t = not $ (t, m) `HS.member` timetable
    insertAndPair (t, m) =
      (
        m,
        (
          Timetable $
          foldl' (flip $ HS.insert . (, m)) timetable $
          fromTo t (t + processing)
        )
      )

countMachines :: Timetable -> Natural
countMachines (Timetable timetable) =
  fromIntegral $
  length $
  hashNub $
  fmap snd $
  toList $
  timetable

timetableEmpty :: Timetable
timetableEmpty = Timetable HS.empty
