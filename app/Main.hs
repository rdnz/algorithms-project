module Main where

import ParseInput.Offline (parseOfflineInput)
import ParseInput.Online (parseOnlineInput)
import ParseInput.Either (parseInput)
import LinearProgram.Model (model)
import Gurobi (solve)
import LinearProgram.InterpretSolution (schedulePatients)
import DisplayTimetable (displayTimetable)
import OnlineAlgorithm qualified

import Options.Applicative
import Std

-- Code in the shape of
-- a $
-- b $
-- c $
-- d
-- is best read bottom up. It corresponds to
-- a(b(c(d)))
-- in languages with a C like syntax.

offlineAlgorithm :: FilePath -> Bool -> IO ()
offlineAlgorithm inputFile verbose =
  (=<<) putText $
  fmap
    (\(machineCount, variables, _) ->
      displayTimetable (schedulePatients variables) machineCount
    ) $
  (=<<) (solve verbose) $
  fmap model $
  (=<<) (maybe (throwString "patients missing") pure) $
  (=<<) (parseOfflineInput inputFile) $
  readFileUtf8 inputFile

onlineAlgorithm :: FilePath -> IO ()
onlineAlgorithm inputFile =
  (=<<) putText $
  fmap (uncurry displayTimetable) $
  fmap OnlineAlgorithm.go $
  (=<<) (maybe (throwString "patients missing") pure) $
  (=<<) (parseOnlineInput inputFile) $
  readFileUtf8 inputFile

-- -- $> offlineAlgorithm "Offline/10-1.txt" False

-- -- $> onlineAlgorithm "Offline/10-1.txt"

options :: ParserInfo (IO ())
options =
  flip info
    fullDesc
    (
      helper
      <*>
      hsubparser
        (
          command
            "offline"
            (flip info mempty
              (flip offlineAlgorithm
                <$>
                  switch
                    (long "verbose" <> short 'v' <> help "Whether to log Gurobi output to stdout")
                <*> argument str (metavar "FILE")
              )
            )
          <>
          command
            "online"
            (flip info mempty $
              onlineAlgorithm <$> argument str (metavar "FILE")
            )
        )
    )

main :: IO ()
main = join (execParser options)
