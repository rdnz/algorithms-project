module LinearProgramming where

import Data.Text.Lazy.Builder (Builder)
import Std hiding (sum)

sum :: NonEmpty Builder -> Builder
sum = foldr1 plus

plus :: Builder -> Builder -> Builder
plus a b = a <> " + " <> b

minus :: Builder -> Builder -> Builder
minus a b = a <> " - " <> b

lessOrEquals :: Builder -> Builder -> Builder
lessOrEquals a b = a <> " <= " <> b

greaterOrEquals :: Builder -> Builder -> Builder
greaterOrEquals a b = a <> " >= " <> b

equals :: Builder -> Builder -> Builder
equals a b = a <> " = " <> b
