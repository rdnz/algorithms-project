# algorithms-project

## Build

1. Install [Gurobi](https://www.gurobi.com/)
2. [Install Stack](https://docs.haskellstack.org/en/stable/README/#how-to-install)
3. Execute
   ```shell
   stack install
   ```

## Run

Execute
```shell
./run online FILE
```
or
```shell
./run offline FILE
```
or
```
./run offline -v FILE
```
logging Gurobi output to stdout in the last case.
