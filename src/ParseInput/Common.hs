module ParseInput.Common where

import Text.Megaparsec hiding (many, some)
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer qualified as L
import Std

type Parse = Parsec Void Text

data Input =
  Input
    {
      processing0 :: Time,
      processing1 :: Time,
      gap :: Time,
      patients :: NonEmpty Patient
    }
  deriving (Show)

data Patient =
  Patient
    {
      interval0Start :: Time,
      interval0Length :: Time,
      delay :: Time,
      interval1Length :: Time
    }
  deriving (Show)

newtype Time =
  Time Natural
  deriving newtype (Show, Eq, Ord, Enum, Num, Hashable)

lexeme :: Parse a -> Parse a
lexeme = L.lexeme (L.space space1 (L.skipLineComment "//") empty)

lexemeWithinLine :: Parse a -> Parse a
lexemeWithinLine =
  L.lexeme (L.space hspace1 (L.skipLineComment "//") empty)

globalParameter :: Parse Time
globalParameter = lexemeWithinLine L.decimal <* eol

patient :: Parse Patient
patient =
  lexeme $
    mkPatient
      <$> (lexemeWithinLine L.decimal <* lexemeWithinLine (char ','))
      <*> (lexemeWithinLine L.decimal <* lexemeWithinLine (char ','))
      <*> (lexemeWithinLine L.decimal <* lexemeWithinLine (char ','))
      <*> lexemeWithinLine L.decimal
    <*
    lookAhead (void eol <|> eof)
  where
    mkPatient a b = Patient a (b - a + 1)

