module DisplayTimetable where

import ParseInput.Common (Time (Time))

import Data.Text.Lazy.Builder qualified as TB
import Data.Text.Lazy.Builder.Int (decimal)
import Std

newtype Machine =
  Machine Natural
  deriving (Show, Hashable, Eq)

data ScheduledPatient = ScheduledPatient Time Machine Time Machine

displayTimetable :: [ScheduledPatient] -> Integer -> Text
displayTimetable patients machineCount =
  toStrict $
  TB.toLazyText $
  foldMap displayPatient patients <> decimal machineCount <> "\n"

displayPatient :: ScheduledPatient -> TB.Builder
displayPatient
  (ScheduledPatient
    (Time time0)
    (Machine machine0)
    (Time time1)
    (Machine machine1)
  )
  =
    intercalate ", " (decimal <$> [time0, machine0, time1, machine1])
    <>
    "\n"
  
